const express = require('express');
const userRouter = express.Router();
const userController = require('./controller');

userRouter.get("/user", userController.getAlluser);

userRouter.post("/registrasi", userController.registrasiUser);

userRouter.post("/login", userController.loginUser);

userRouter.get("/detail/:idUser", userController.getSingleUser);

userRouter.put("/updatebio/:idUser", userController.updateUserBio);

userRouter.post("/insertbio", userController.insertUserBio);

userRouter.post("/game", userController.insertGame);

userRouter.get("/history/:idUser", userController.getHistory);
  
module.exports = userRouter;