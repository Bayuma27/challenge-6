const md5 = require("md5");
const db = require('../db/models');
const { json, Op } = require("sequelize");

const userList = [];

class userModel{
//Mendapatkan semua data
getAllusers = async () =>{
    const allUsers = await db.User.findAll({ include: [db.UserBio, db.GameHistory], });
    return allUsers;
};

//Mendapatkan single user
getSingleUser = async (idUser) => {
  return await db.User.findOne({include: [db.UserBio,db.GameHistory], where: {id:idUser}});
};

//Mengupdate UserBio
updateUserBio = async (idUser, fullname, address, phoneNumber, Age) => {
  return await db.UserBio.update(
    {
      fullname: fullname, 
      address: address, 
      phoneNumber: phoneNumber, 
      Age: Age
    },
    {where: { user_id: idUser }}
    );
};
    
// method registrasi username
isUserRegistered = async (dataRequest) => {
  const existData = await db.User.findOne({
    where: {
      [Op.or]: [
        {username: dataRequest.username},
        {email: dataRequest.email},
      ], 
     },
  });

  if(existData){
    return true;
  } else {
    return false;
  }
};

//Method registrasi UserBio
bioUserRegistered = async (dataRequest) => {
  const existDataBio = await db.UserBio.findOne({
    where: {
      [Op.or]: [
        {fullname: dataRequest.fullname},
        {address: dataRequest.address},
        {phoneNumber: dataRequest.phoneNumber},
        {Age: dataRequest.Age},
        {user_id: dataRequest.user_id},
      ], 
     },
  });

  if(existDataBio){
    return true;
  } else {
    return false;
  }
};

//Menambahkan data baru
recordNewData = (dataRequest) => {
    console.log(dataRequest);
    db.User.create({
    username: dataRequest.username,
    email: dataRequest.email,
    password: md5(dataRequest.password),
  });
};

//Menambahkan UserBio
recordNewDataBios = (dataRequestBio) => {
  console.log(dataRequestBio);
  db.UserBio.create({
    fullname: dataRequestBio.fullname,
    address: dataRequestBio.address,
    phoneNumber: dataRequestBio.phoneNumber,
    Age: dataRequestBio.Age,
    user_id: dataRequestBio.user_id,
});
}

//Menambahkan History Game
recordGame = (dataRequestGame) => {
  console.log(dataRequestGame);
  db.GameHistory.create({
    user_id: dataRequestGame.user_id,
    status: dataRequestGame.status,
    time: dataRequestGame.time,
});
}
//Login
userLogin = async (username, email, password) => {
const LoginUser = await db.User.findOne({
  where: {
    username: username, 
    email: email, 
    password: md5(password) 
  },
});

return LoginUser;
};

getGameHistory = async (idUser) => {
  return await db.GameHistory.findOne({ include: [db.User], where: {id:idUser} });
};
}

module.exports = new userModel();