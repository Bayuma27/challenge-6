const userModel = require('./model');

class UserController {
    //Mendapatkan semua data User
    getAlluser = async (req, res) => {
      const allUsers = await userModel.getAllusers();
      return res.json(allUsers);
    };

    //Mendapatkan detail user menggunakan id user
    getSingleUser = async (req, res) => {
      const {idUser} = req.params;
      try {
        const users = await userModel.getSingleUser(idUser);
        if (users) {
          return res.json(users);
        } else {
          res.statusCode = 400;
          return res.json({message: `User id tidak ditemukan : ${idUser}`});
        }
      }catch (error) {
        res.statusCode = 400;
        return res.json({message: `User id tidak ditemukan : ${idUser}`});
      }
    };

    //Mendapatkan history permainan berdasarkan ID
    getHistory = async (req, res) => {
      const {idUser} = req.params;
      try {
        const gamehis = await userModel.getGameHistory(idUser);
        console.log(gamehis);
        if (gamehis) {
          return res.json(gamehis);
        } else {
          res.statusCode = 400;
          return res.json({message: `User id tidak ditemukan : ${idUser}`});
        }
      } catch (error) {
        res.statusCode = 400;
        return res.json({message: `User id tidak ditemukan : ${idUser}`});
      }
    };

    //Update User Bio berdasarkan ID
    updateUserBio = async (req,res) => {
      const { idUser } = req.params;
      const { fullname } = req.body;
      const { address } = req.body;
      const { phoneNumber } = req.body;
      const { Age } = req.body;

      const updateUser = await userModel.updateUserBio(idUser, fullname, address, phoneNumber, Age);

      return res.json(updateUser);
    };

    //Registrasi User
    registrasiUser = async (req, res) => {
        const dataRequest = req.body;
        if (dataRequest.username === undefined || dataRequest.username === "") {
          res.statusCode = 400;
          return res.json({ message: "Username is Invalid" });
        }
        if (dataRequest.email === undefined || dataRequest.email === "") {
          res.statusCode = 400;
          return res.json({ message: "Email is Invalid" });
        }
        if (dataRequest.password === undefined || dataRequest.password === "") {
          res.statusCode = 400;
          return res.json({ message: "Password is Invalid" });
        }
      
        const existData = await userModel.isUserRegistered(dataRequest);
      
        if (existData) {
          return res.json({ message: "Username or email is exist!" });
        }
      
        //Menambahkan data baru
        userModel.recordNewData(dataRequest);
        return res.json({ message: "New User Is Recorded"});
    };

    //Insert UserBio
    insertUserBio = async (req, res) => {
      const dataRequestBio = req.body;
      if (dataRequestBio.fullname === undefined || dataRequestBio.fullname === "") {
        res.statusCode = 400;
        return res.json({ message: "Fullname is Invalid" });
      }
      if (dataRequestBio.address === undefined || dataRequestBio.address === "") {
        res.statusCode = 400;
        return res.json({ message: "Address is Invalid" });
      }
      if (dataRequestBio.phoneNumber === undefined || dataRequestBio.phoneNumber === "") {
        res.statusCode = 400;
        return res.json({ message: " Phone NUmber is Invalid" });
      }
      if (dataRequestBio.Age === undefined || dataRequestBio.Age === "") {
        res.statusCode = 400;
        return res.json({ message: "Age is Invalid" });
      }
      if (dataRequestBio.user_id === undefined || dataRequestBio.user_id === "") {
        res.statusCode = 400;
        return res.json({ message: "user_id is Invalid" });
      }
    
      const existDataBio = await userModel.bioUserRegistered(dataRequestBio);
    
      if (existDataBio) {
        return res.json({ message: "Fullname or Phone Number is exist!" });
      }
    
      //Menambahkan data baru
      userModel.recordNewDataBios(dataRequestBio);
      return res.json({ message: "New UserBio Is Recorded"});
    };

    //Record History Game
    insertGame = async (req, res) => {
      const dataRequestGame = req.body;
      const existGame = await userModel.recordGame(dataRequestGame);
      //Menambahkan data baru
      userModel.recordGame(dataRequestGame);
      return res.json({ message: "New Game History Is Recorded"});
    };

    //Login User
    loginUser = async (req, res) => {
        const { username, email, password } = req.body;
        const dataLogin = await userModel.userLogin(username, email, password)
      
        if (dataLogin) {
        return res.json(dataLogin);
        } else {
        return res.json({ message: "Invalid credential" });
        }
    };

}

module.exports = new UserController();